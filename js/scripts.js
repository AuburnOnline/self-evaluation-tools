/*!
* Start Bootstrap - Modern Business v5.0.7 (https://startbootstrap.com/template-overviews/modern-business)
* Copyright 2013-2023 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-modern-business/blob/master/LICENSE)
*/
// This file is intentionally blank
// Use this file to add JavaScript to your project

$(document).ready(function () {
    // Set the first two buttons as active when the page is loaded
    $('input[name="filterLetter"]').eq(0).prop('checked', true).trigger('change');
    //$('input[name="filterLetter"]').eq(1).prop('checked', true).trigger('change');
    $('input[name="filterCategory"]').eq(0).prop('checked', true).trigger('change');
    //$('input[name="filterCategory"]').eq(1).prop('checked', true).trigger('change');

    $('input[name="filterLetter"]').change(function () {
        var selectedLetter = $(this).val();
        var selectedCategory = $('input[name="filterCategory"]:checked').val();
        filterEntries(selectedLetter, selectedCategory);
    });

    $('input[name="filterCategory"]').change(function () {
        var selectedLetter = $('input[name="filterLetter"]:checked').val();
        var selectedCategory = $(this).val();
        filterEntries(selectedLetter, selectedCategory);
    });

    // Trigger initial filtering based on default selections
    var defaultLetter = $('input[name="filterLetter"]:checked').val();
    var defaultCategory = $('input[name="filterCategory"]:checked').val();
    filterEntries(defaultLetter, defaultCategory);
});

function filterEntries(selectedLetter, selectedCategory) {
    var entries = $('.glossary-entry');
    entries.hide();

    entries.each(function () {
        var word = $(this).find('.word').text();
        var category = $(this).find('.category').text().slice(1);

        var entryMatchesLetter = selectedLetter === '' || word.charAt(0).toUpperCase() === selectedLetter;
        var entryMatchesCategory = !selectedCategory || selectedCategory === '' || category.toLowerCase() === selectedCategory.toLowerCase();

        if (selectedCategory === '#' && selectedLetter === '#') {
            $(this).show();
        } else if (selectedCategory === '#' && selectedLetter !== '#') {
            if (entryMatchesLetter) {
                $(this).show();
            }
        } else if (selectedCategory !== '#' && selectedLetter === '#') {
            if (category.toLowerCase() === selectedCategory.toLowerCase()) {
                $(this).show();
            }
        } else if (entryMatchesLetter && entryMatchesCategory) {
            $(this).show();
        }
    });

    // Remove active class from all filter buttons
    $('input[name="filterLetter"]').parent().removeClass('active');
    $('input[name="filterCategory"]').parent().removeClass('active');

    // Add active class to the selected filter buttons
    if (selectedLetter) {
        $('input[name="filterLetter"][value="' + selectedLetter + '"]').parent().addClass('active');
    }
    if (selectedCategory) {
        $('input[name="filterCategory"][value="' + selectedCategory + '"]').parent().addClass('active');
    }

}
